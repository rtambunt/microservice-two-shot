import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVO

# Polling is our way of grabbing locations data from the wardrobe project into our hats_rest django app

def get_locations():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)

    # Access next layer of content json
    for location in content["locations"]:
        # Create new LocatonVO Object
        print(location)
        LocationVO.objects.update_or_create(
            import_href=location["href"],
            defaults={"closet_name": location["closet_name"], "section_number": location["section_number"], "shelf_number": location["shelf_number"]}
        )
        print(LocationVO.objects.all())
        print('Working poller!')


def poll():
    while True:
        print("Hats poller polling for data")
        try:
            get_locations()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
