from django.db import models

# Location Model has the following properties (Located in wardrobe_api)
# closet_name = models.CharField(max_length=100)
# section_number = models.PositiveSmallIntegerField()
# shelf_number = models.PositiveSmallIntegerField()
#
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

class Hat(models.Model):
    fabric = models.CharField(max_length=150)
    style_name = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    img_url = models.URLField()
    location = models.ForeignKey(LocationVO, related_name="hats", on_delete=models.CASCADE)
