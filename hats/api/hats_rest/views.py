from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "style_name", "fabric", "color", "img_url"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "img_url",
        "location"
    ]

    encoders = {
        "location": LocationVODetailEncoder()
    }


@require_http_methods(["GET", "POST"])
def hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            location_href = f"/api/locations/{location_vo_id}/"
            hats = Hat.objects.filter(location__import_href=location_href)
        else:
            hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatListEncoder)
    else:
        # Handles POST request/creating a hat
        content = json.loads(request.body)
        try:
            # Assign variable to location href assigned passed into hat item
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location

        # Return error message if location doesn't exist
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)

        hat = Hat.objects.create(**content)

        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)

@require_http_methods(["GET", "DELETE"])
def hat_details(request, id):
    # Check if hat exists
    try:
        hat = Hat.objects.get(id=id)
    except Hat.DoesNotExist:
        return JsonResponse({"message": "Hat does not exist"}, status=400)

    # --- Get hat details ---
    if request.method == "GET":
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)

    # --- Deleting a hat ---
    else:
        # IMPORTANT
        # .delete() query set operation returns
        # 1. # of objects deleted
        # 2. A dictionary with the number of deletions per type
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
