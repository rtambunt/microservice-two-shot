from django.urls import path
from .views import hats, hat_details

urlpatterns = [
    path('hats/', hats, name="create_hat"),
    path('locations/<int:location_vo_id>/hats/', hats, name="list_hats"),
    path('hats/<int:id>/', hat_details, name="hat_details")
]
