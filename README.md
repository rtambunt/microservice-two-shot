# Wardrobify

Team:

- Brandon - shoes
- Robbie - hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here. its almost lunch time

To start off we need to create the models for the Bin with closet_name, bin_number, bin_size. Then we create the shoe model for the manufacturer, model_name, color, picture_url, and bin.

After creating the models and for whenever we mess around with the model page, we have to make migrations.

Then we create the view functions that we want to use. These will be functions that will relate to us using insomnia for creating bins and creating shoes.

In order for our insomnia to be able to create the bins we want, we have to create the poller.py file. This is so we can create the bin with the default values of what we had it for the bin model.

Now we have to make sure that creating bin, listing bins, then creating shoes and listing shoes works.


Now to move onto react, we start with the app.js to create the paths for shoe list and shoe form.

I also added the link tags to the nav.js so that we are able to access the form and list from the header drop down menu.

Now to start with the shoelist, we have to make the ShoesList constant along with a return. The return statement will have all the html related to the shoe list. Later on we would add the delete function along with a button to delete specific shoes.

Then we will create the shoesform which will be a little bit more complicated. We have to establish variables for the description of the shoe that we want the user to enter (manufacturer, model name, bin, color and a pictrure url).

For the variables, we will have a bin variable and a bins variable. the bins variable will have a tuple as the parameter for the usestate method so that we are able to keep naming conventions consistent. This is what we will be using to fetch the data if the response is okay. This relates to the drop down menu for bins.

With these variables, we also have to have a usestate function for each so that we are able to save the state of the form whenever a user types something inside of the box.

Along with the handle change functions we also need to have a function for handling the submit. Within this function we are assigning variables to the data that is entered in. Also within this function, we want the data to be saved to the database as well as reset the inputted values to empty strings.

After we have all of these completed we should have full functionality of our website. There should be no error while creating a shoe.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

Robbies change
