import React, { useEffect, useState } from 'react';


export function ShoesForm(props) {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModelName] = useState('');
    const [bin, setBin] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPicture] = useState('')

    const handlePictureUrl = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleModelName = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = model_name;
        data.bin = bin;
        data.color = color;
        console.log(data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setManufacturer('');
            setModelName('');
            setBin('');
            setColor('')
            setPicture('')
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="col">
                <h1>Add a Shoe</h1>
                <form onSubmit={handleSubmit} id="add-shoe-form">
                    <div>
                        <label htmlFor="manufacturer">Manufacturer</label>
                        <input
                            onChange={handleManufacturer}
                            required
                            type="text"
                            placeholder="Manufacturer"
                            id="manufacturer"
                        />
                    </div>
                    <div>
                        <label htmlFor="model_name">Model Name</label>
                        <input
                        onChange={handleModelName}
                        value={model_name}
                        required
                        type="text"
                        placeholder="Model Name"
                        id="model_name"
                        />
                    </div>
                    <div>
                        <label htmlFor="color">Color</label>
                        <input
                        onChange={handleColor}
                        value={color}
                        required
                        type="text"
                        placeholder="Color"
                        id="color"
                        />
                    </div>
                    <div>
                        <label htmlFor="picture_url">Picture Url</label>
                        <input
                        onChange={handlePictureUrl}
                        value={picture_url}
                        required
                        type="text"
                        placeholder="Picture Url"
                        id="picture_url"
                        />
                    </div>
                    <div>
                        <select onChange={handleBin}
                        value={bin}
                        name="bin"
                        id="bin"
                        required
                        className="form-select"
                        >
                            <option value="">Choose a bin</option>
                                {bins.map((bin) => {
                                    return (
                                        <option key={bin.href} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    );
                                })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Add</button>
                </form>
            </div>
        </div>
    );
};
