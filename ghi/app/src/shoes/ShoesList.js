import React from "react";

export const ShoesList = ({ shoes }) => {
    const handleDelete = async(shoeId) => {
        const deleteShoeUrl = `http://localhost:8080/api/shoes/${shoeId}`;

        const fetchConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(deleteShoeUrl, fetchConfig);

        if (response.ok) {
            alert("Shoe deleted! Reload to see updated list");
        }
    };
    return (
        <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Picture</th>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {shoes?.map((shoe) => {
                    return (
                        <tr key={shoe.id}>
                            <td> <img src={ shoe.picture_url } width="100" alt=''/> </td>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.bin}</td>
                            <td><button onClick={() => handleDelete(shoe.id)}>Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    );
};
