import React, { useEffect, useState } from "react";

export const HatsForm = () => {
  const [styleName, setStyleName] = useState("");
  const [fabric, setFabric] = useState("");
  const [color, setColor] = useState("");
  const [imgUrl, setImgUrl] = useState("");
  const [location, setLocation] = useState("");
  const [locations, setLocations] = useState([]);

  // Getting our Location List from API
  useEffect(() => {
    async function getLocations() {
      const locationsUrl = "http://localhost:8100/api/locations/";
      const response = await fetch(locationsUrl);

      if (response.ok) {
        const responseData = await response.json();
        setLocations(responseData.locations);
      }
    }
    getLocations();
  }, []);

  const handleStyleNameChange = (e) => {
    setStyleName(e.target.value);
  };

  const handleFabricChange = (e) => {
    setFabric(e.target.value);
  };

  const handleColorChange = (e) => {
    setColor(e.target.value);
  };

  const handleImgUrlChange = (e) => {
    setImgUrl(e.target.value);
  };

  const handleLocationChange = (e) => {
    setLocation(e.target.value);
  };

  //   --- Submitting! ---
  const handleSubmit = async (e) => {
    e.preventDefault();

    // **** MAKE SURE newHatData ACCURATELY REFLECTS JSON BODY FORMAT WHEN
    // CREATING A POST REQUEST (via insomnia or elsewhere)

    // Sample json body
    // {
    //     "fabric": "corduroy",
    //     "style_name": "baseball cap",
    //     "color": "beige",
    //     "img_url": "https://cdna.lystit.com/photos/urbanoutfitters/141fb40a/47-Blue-Uo-Exclusive-Mlb-New-York-Yankees-Cord-Cleanup-Baseball-Hat.jpeg",
    //     "location": "/api/locations/1/"
    // }

    const newHatData = {
      fabric,
      style_name: styleName,
      color,
      img_url: imgUrl,
      location,
    };

    // Making a POST request to make a new hat
    const createHatUrl = "http://localhost:8090/api/hats/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(newHatData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(createHatUrl, fetchConfig);

    if (response.ok) {
      const newHat = await response.json();
      //   console.log our response - Optional
      //   console.log(newHat);

      // -- alert user that hat was created --
      alert("New Hat Created!");
    }

    //  --- Reset our form ---
    setFabric("");
    setStyleName("");
    setColor("");
    setImgUrl("");
    setLocation("");
  };

  return (
    <div className="row">
      <div className="col">
        <h1>Add a Hat</h1>
        <form onSubmit={handleSubmit} id="add-hat-form">
          <div>
            <label htmlFor="style-name">Style Name</label>
            <input
              onChange={handleStyleNameChange}
              value={styleName}
              required
              type="text"
              placeholder="Style Name"
              id="style-name"
            />
          </div>
          <div>
            <label htmlFor="fabric">Fabric</label>
            <input
              onChange={handleFabricChange}
              value={fabric}
              required
              type="text"
              placeholder="fabric"
              id="fabric"
            />
          </div>
          <div>
            <label htmlFor="color">Color</label>
            <input
              onChange={handleColorChange}
              value={color}
              required
              type="text"
              placeholder="color"
              id="color"
            />
          </div>
          <div>
            <label htmlFor="imgUrl">Image URL</label>
            <input
              onChange={handleImgUrlChange}
              value={imgUrl}
              required
              type="text"
              placeholder="Image URL"
              id="imgUrl"
            />
          </div>
          <div className="mb-3">
            <select
              onChange={handleLocationChange}
              value={location}
              required
              name="location"
              id="location"
              className="form-select"
            >
              <option value="">Choose a location</option>
              {locations.map((location) => {
                return (
                  <option key={location.id} value={location.href}>
                    {location.closet_name}
                  </option>
                );
              })}
            </select>
          </div>
          <button className="btn btn-primary">Add</button>
        </form>
      </div>
    </div>
  );
};
