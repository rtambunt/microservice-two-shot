import React from "react";

export const HatList = ({ hats }) => {
  const handleDelete = async (hatId) => {
    const deleteHatUrl = `http://localhost:8090/api/hats/${hatId}/`;

    const fetchConfig = {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(deleteHatUrl, fetchConfig);

    if (response.ok) {
      alert("Hat deleted! Reload to see updated list");
    }
  };
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Image</th>
          <th>Style Name</th>
          <th>Fabric</th>
          <th>Color</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        {/* Optional chaining ?. to deal with browser console error */}
        {hats?.map((hat) => {
          return (
            <tr>
              <td>
                <img src={hat.img_url} width="100px" alt="" />
              </td>
              <td>{hat.style_name}</td>
              <td>{hat.fabric}</td>
              <td>{hat.color}</td>
              <td>{hat.location}</td>
              <button onClick={() => handleDelete(hat.id)}>Delete</button>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};
