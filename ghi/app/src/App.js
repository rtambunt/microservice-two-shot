import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import { HatList } from "./hats/HatList";
import { HatsForm } from "./hats/HatsForm";
import { ShoesList } from "./shoes/ShoesList";
import { ShoesForm } from "./shoes/ShoesForm";

function App(props) {
  const hatList = props.hatList;
  const shoeList = props.shoeList
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatList hats={hatList} />} />
            <Route path="new-hat" element={<HatsForm />} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoesList shoes={shoeList} />} />
            <Route path="new-shoe" element={<ShoesForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
