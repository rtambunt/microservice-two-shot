import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// -- Getting data from our API --

async function loadAPIData() {
  const hatResponse = await fetch("http://localhost:8090/api/hats/");
  const shoeResponse = await fetch("http://localhost:8080/api/shoes/");

  if (hatResponse.ok) {
    const hatData = await hatResponse.json();
    root.render(
      <React.StrictMode>
        <App hatList={hatData.hats} />
      </React.StrictMode>
    );
  } else {
    console.error(hatResponse);
  }

  if (shoeResponse.ok) {
    const shoeData = await shoeResponse.json();
    root.render(
      <React.StrictMode>
        <App shoeList={shoeData.shoes} />
      </React.StrictMode>
    );
  } else {
    console.error(shoeResponse)
  }
  }
loadAPIData();
